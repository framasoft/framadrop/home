[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/48px-GitLab_Logo.svg.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Framadrop est un service en ligne de partage de fichier que [Framasoft](https://framasoft.org) propose sur le site : <https://framadrop.org>.

Il repose sur le logiciel [Lufi](https://framagit.org/luc/lufi).

Si vous souhaitez traduire la page d’accueil, allez sur <https://weblate.framasoft.org/projects/framadrop/homepage/>.

* * *

Framadrop is an online service provided by [Framasoft](https://framasoft.org) on <https://framadrop.org>.

It’s based on [Lufi](https://framagit.org/luc/lufi) .

If you want to translate the homepage, go on <https://weblate.framasoft.org/projects/framadrop/homepage/>.
